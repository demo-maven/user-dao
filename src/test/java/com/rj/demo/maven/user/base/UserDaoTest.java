package com.rj.demo.maven.user.base;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.rj.demo.maven.user.entity.User;
import com.rj.demo.maven.user.mapper.UserMapper;

public class UserDaoTest extends BaseTest {
	@Autowired
	private UserMapper userMapper;
	
	@Test
	public void selectByPrimaryKey(){
		String userId = "340219586710339584";
		User user = userMapper.selectByPrimaryKey(userId);
		
		System.out.println(user);
	};

}
