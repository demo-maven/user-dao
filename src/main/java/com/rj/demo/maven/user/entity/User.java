package com.rj.demo.maven.user.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.core.serializer.Serializer;

public class User implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3260325581177149185L;

	private Integer id;

    private Byte gender;

    private Byte state;

    private String password;

    private String phone;

    private String userId;

    private String realName;

    private String nickname;

    private String headPic;

    private String email;

    private Date birthday;

    private Date createAt;

    private Date updateAt;

    private String organizationId;

    private Date lastLoginAt;

    private Long pace;

    private Long sportsExperience;

    private Long weeklyRun;

    private Integer height;

    private Long weight;

    private String bizUserId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Byte getGender() {
        return gender;
    }

    public void setGender(Byte gender) {
        this.gender = gender;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public String getHeadPic() {
        return headPic;
    }

    public void setHeadPic(String headPic) {
        this.headPic = headPic == null ? null : headPic.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId == null ? null : organizationId.trim();
    }

    public Date getLastLoginAt() {
        return lastLoginAt;
    }

    public void setLastLoginAt(Date lastLoginAt) {
        this.lastLoginAt = lastLoginAt;
    }

    public Long getPace() {
        return pace;
    }

    public void setPace(Long pace) {
        this.pace = pace;
    }

    public Long getSportsExperience() {
        return sportsExperience;
    }

    public void setSportsExperience(Long sportsExperience) {
        this.sportsExperience = sportsExperience;
    }

    public Long getWeeklyRun() {
        return weeklyRun;
    }

    public void setWeeklyRun(Long weeklyRun) {
        this.weeklyRun = weeklyRun;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    public String getBizUserId() {
        return bizUserId;
    }

    public void setBizUserId(String bizUserId) {
        this.bizUserId = bizUserId == null ? null : bizUserId.trim();
    }

	@Override
	public String toString() {
		return "User [id=" + id + ", gender=" + gender + ", state=" + state + ", password=" + password + ", phone="
				+ phone + ", userId=" + userId + ", realName=" + realName + ", nickname=" + nickname + ", headPic="
				+ headPic + ", email=" + email + ", birthday=" + birthday + ", createAt=" + createAt + ", updateAt="
				+ updateAt + ", organizationId=" + organizationId + ", lastLoginAt=" + lastLoginAt + ", pace=" + pace
				+ ", sportsExperience=" + sportsExperience + ", weeklyRun=" + weeklyRun + ", height=" + height
				+ ", weight=" + weight + ", bizUserId=" + bizUserId + "]";
	}

}