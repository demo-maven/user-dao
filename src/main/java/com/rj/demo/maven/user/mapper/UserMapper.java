package com.rj.demo.maven.user.mapper;

import com.rj.demo.maven.user.entity.User;

public interface UserMapper {
	public int deleteByPrimaryKey(Integer id);
	
	public int insert(User record);
	
	public int insertSelective(User record);
	
	public User selectByPrimaryKey(String userId);
	
	public int updateByPrimaryKeySelective(User record);
	
	public int updateByPrimaryKey(User record);
}